package net.ligamx.firstJavaProyect.run;

public class Accesibilidad {
	
	public void saludo() {
		System.out.println("Hi");
	}
	
	public void despedida() {
		System.out.println("GoodBye");
	}
	
	public void groseria() {
		System.out.println("Buena Salvada");
	}
	
	public static void main(String[]args) {
		
		UserUtils userUtils = new UserUtils();
		Accesibilidad accesibilidad = new Accesibilidad();
		
		String optionContinue = "n";
		
		do {
			
			char option = userUtils.giveMeYourOptionChar("a) Saludo \n b) Despedida \n c) Groseria");
			
			switch(option) {
			
			case 'a':
				accesibilidad.saludo();
				break;
				
			case 'b':
				accesibilidad.despedida();
				break;
				
			case 'c':
				accesibilidad.groseria();
				break;
				
				default:
					System.out.println("Invalid option");
					break;
			}
			optionContinue = userUtils.doYouWishContinue();
		
		}while (optionContinue.equals("y"));
		
		System.out.println("Finished");
	}
	

}
