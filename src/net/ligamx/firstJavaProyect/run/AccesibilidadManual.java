package net.ligamx.firstJavaProyect.run;

public class AccesibilidadManual {

	public static void main(String[] args) {

		UserUtils userUtils = new UserUtils();
		Operaciones operaciones = new Operaciones();

		String optionContinue = "n";
		do {
			

			int primero = userUtils.giveMeFirst();
			int segundo = userUtils.giveMeSecond();


			char option = userUtils.giveMeYourOptionChar("a) Suma \n b) Resta \n c) Multiplicacion \n d) Division");



			switch (option) {
			case 'a': 
				operaciones.suma(primero, segundo);
				break;

			case 'b':
				operaciones.resta(primero, segundo);
				break;
				
			case 'c':
				operaciones.multiplicacion(primero, segundo);
				break;
				
			case 'd':
				operaciones.division(primero, segundo);
				break;
				
			default:
				System.out.println("opcion invalida");
			}

			optionContinue = userUtils.doYouWishContinue();

		} while (optionContinue.equals("y"));
	}

}
