package net.ligamx.firstJavaProyect.run;

import java.util.Scanner;

public class AreaFiguras {

	public static void main(String[] args) {
		UserUtils userUtils = new UserUtils();

		Scanner scanner = new Scanner(System.in);

		double radio;
		double valor1;
		double valor2;
		double perimetro;
		double apotema;
		double base;
		double altura;

		String optionContinue = "n";
		do {

			char option = userUtils.giveMeYourOptionChar(
					" a) circulo \n b) cuadrado \n c) pentagono \n d) rombo \n e) trapecio \n f) triangulo");

			switch (option) {

			case 'a':
				System.out.println("Ingresa el radio:");
				radio = scanner.nextDouble();
				System.out.println("El area del circulo es= " + Math.PI * Math.pow(radio, 2));
				break;

			case 'b':
				System.out.println("Ingresa el valor de el lado:");
				valor1 = scanner.nextDouble();
				System.out.println("El area del cuadrado es = " + valor1 * valor1);
				break;

			case 'c':
				System.out.println("Ingresa el perimetro: ");
				perimetro = scanner.nextDouble();
				System.out.println("Ingrsa el apotema: ");
				apotema = scanner.nextDouble();
				System.out.println("El area del pentagono es = " + perimetro * apotema / 2);
				break;

			case 'd':
				System.out.println("Ingresa la diagonal mayor:");
				valor1 = scanner.nextDouble();
				System.out.println("Ingresa la diagonal menor:");
				valor2 = scanner.nextDouble();
				System.out.println("El area del rombo es = " + valor1 * valor2 / 2);
				break;

			case 'e':
				System.out.println("Ingresa la base mayor:");
				valor1 = scanner.nextDouble();
				System.out.println("Ingresa la base menor:");
				valor2 = scanner.nextDouble();
				System.out.println("Ingresa la altura:");
				altura = scanner.nextDouble();
				System.out.println("El area del trapecio es = " + (valor1 + valor2) * altura / 2);
				break;

			case 'f':
				System.out.println("Ingresa la base:");
				base = scanner.nextDouble();
				System.out.println("Ingresa la altura:");
				altura = scanner.nextDouble();
				System.out.println("El area del triangulo es = " + base * altura / 2);
				break;

			default:
				System.out.println("opcion invalida");
			}
			optionContinue = userUtils.doYouWishContinue();
		} while (optionContinue.equals("y"));

	}

}
