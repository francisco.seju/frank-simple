package net.ligamx.firstJavaProyect.run;

import java.util.Scanner;

public class Cajero {

	public static void main(String[] args) {

		UserUtils userUtils = new UserUtils();
		Cajero cajero = new Cajero();
		Movimientos movimientos = new Movimientos();

		Scanner scanner = new Scanner(System.in);

		System.out.println("Hola usted cuenta con 10,000 pesos");

		String continuar = "n";
		int saldo = 10000;
		int abonar;
		int retirar;

		do {

			char opcion = userUtils.giveMeYourOptionChar("\n a) abonar \n b) retirar");

			switch (opcion) {

			case 'a':
				System.out.println("Ingrese la cantidad que desea abonar:");
				abonar = scanner.nextInt();
				saldo = movimientos.suma(abonar, saldo);
				break;

			case 'b':
				System.out.println("Ingrese la cantidad que desea retirar;");
				retirar = scanner.nextInt();
				saldo = movimientos.resta(saldo, retirar);
				break;

			default:
				System.out.println("opcion invalida");
			}

			continuar = userUtils.doYouWishContinue();
		} while (continuar.equals("y"));
		System.out.println("Adios");

	}

}
