package net.ligamx.firstJavaProyect.run;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		String respuesta;

		do {

			int numero;
			double factorial = 1;

			System.out.println("Ingresa el nuemero del que deseas saber el Factorial:");
			numero = scanner.nextInt();

			{
				for (int i = 1; i <= numero; i++)
					factorial = factorial * i;

				System.out.println("El factorial de " + numero + " es:" + factorial);

			}

			System.out.println("Desea hacer el Factorial de otro numero? <<y>> <<n>>");
			respuesta = scanner.next();

		} while (respuesta.equals("y"));
		System.out.println("Adios");

	}
}
