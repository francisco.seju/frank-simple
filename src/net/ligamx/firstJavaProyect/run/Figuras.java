package net.ligamx.firstJavaProyect.run;

import java.util.Scanner;

public class Figuras {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		int figura;

		System.out.println("Cuantos lados tiene tu figura?");
		figura = scanner.nextInt();

		if (figura > 0 && figura < 3) {
			System.out.println("No mames necesito mas lados");
		} else if (figura == 3) {
			System.out.println("Es un: Triangulo");
		} else if (figura == 4) {
			System.out.println("Es un: Cuadrado");
		} else if (figura == 5) {
			System.out.println("Es un: Pentagono");
		} else if (figura == 6) {
			System.out.println("Es un: Hexagono");
		} else if (figura == 7) {
			System.out.println("Es un: Heptagono");
		} else {
			System.out.println("Solo me se hasta Heptagono");
		}
	}

}
