package net.ligamx.firstJavaProyect.run;

import java.util.Scanner;

public class Invoke {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		boolean blond;
		boolean haveMoney;
		boolean isFromChalco;
		int age;
		
		System.out.println("Are you blond ? <<y>> <<n>>");
		String blondString = scanner.next();
		
		if (blondString.equals("y")) {
			blond = true;
		} else {
			blond = false;
		}
		
		System.out.println("Do you have money? <<y>> <<n>>");
		String moneyString = scanner.next();
		
		if (moneyString.equals("y")) {
			haveMoney = true;
		} else {
			haveMoney = false;
		}
		
		System.out.println("Are you from Chalco? <<y>> <<n>>");
		String chalcoString = scanner.next();
		
		if (chalcoString.equals("y")) {
			isFromChalco = true;
		} else {
			isFromChalco = false;
		}
		
		System.out.println("How old are you");
		age = scanner.nextInt();
		
		
		if ( ( (blond || haveMoney) || (isFromChalco) ) && age >=18) {
			System.out.println("let in");
		} else {
			System.out.println("let not in");
		}

	}
}
