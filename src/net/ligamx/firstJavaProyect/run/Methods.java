package net.ligamx.firstJavaProyect.run;

import java.util.Scanner;

public class Methods {
	
	static Scanner scanner = new Scanner(System.in);
	
	public static int addition(int numberOne, int numberTwo) {
		return numberOne + numberTwo;
	}
	
	public static int substraction(int numberOne, int numberTwo) {
		return numberOne - numberTwo;
	}
	
	public static double divide(int numberOne, int numberTwo) {
		return numberOne / numberTwo;
	}
	
	
	
	public static String chain() {
		return "Give me first number:";
	}
	
	public static int giveMeFirst() {
		System.out.println(chain());
		return scanner.nextInt();
	}
	
	public static int giveMeSecond() {
		System.out.println("Give me Second number");
		return scanner.nextInt();
	}
	
	public static void main(String[]args) {
		
		int numberOne = giveMeFirst();
		int numberTwo = giveMeSecond();
		
		System.out.println("addition =" + addition(numberOne, numberTwo));
		
		System.out.println("substarction =" + substraction(numberOne, numberTwo));
		
		System.out.println("divide =" + divide(numberOne, numberTwo));
		
	}

}
