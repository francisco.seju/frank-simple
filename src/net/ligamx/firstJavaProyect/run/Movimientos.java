package net.ligamx.firstJavaProyect.run;

import java.util.Scanner;

public class Movimientos {

	static Scanner scanner = new Scanner(System.in);

	public int suma(int numero1, int saldo) {

		int result = numero1 + saldo;

		System.out.println("Ahora usted cuenta con =" + result);

		return result;

	}

	public int resta(int saldo, int numero2) {

		int result = saldo - numero2;

		System.out.println("Ahora a usted le quedan =" + result);

		return result;
	}
}
