package net.ligamx.firstJavaProyect.run;

import java.util.Scanner;

public class Operaciones {
	
   static Scanner scanner = new Scanner(System.in);
   
   public static void suma(int numero1, int numero2) {
	   
	   int result = numero1 + numero2;
	   
	   System.out.println("Resultado de la suma =" + result);
	   
   }
   
   public static void resta(int numero1, int numero2) {
	   
	   int result = numero1 - numero2;
	   
	   System.out.println("Resultado de la resta =" + result);
	   
   }
   
   public static void division(double numero1, double numero2) {
	   
	   double result = numero1 / numero2;
	   
	   System.out.println("Resultado de la division =" + result);
	   
   }
   
   public static void multiplicacion(int numero1, int numero2) {
	   
	   int result = numero1 * numero2;
	   
	   System.out.println("Resultado de la multiplicacion =" + result);
	   
   }
   public static void residuo(double numero1, double numero2) {
	   
	   double result = numero1 % numero2;
	   
	   System.out.println("Residuo =" + result);
	   
   }
   
   public static void main(String[] args) {
	   System.out.println("Ingresa el primer numero :");
	   int numero1 = scanner.nextInt();
	   
	   System.out.println("Ingresa el segundo numero :");
	   int numero2 = scanner.nextInt();
	   
	   suma(numero1, numero2);
	   resta(numero1, numero2);
	   division(numero1, numero2);
	   multiplicacion(numero1, numero2);
	   residuo(numero1, numero2);
   }
 
}
