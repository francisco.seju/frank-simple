package net.ligamx.firstJavaProyect.run;

import java.util.Scanner;

public class UserUtils {
	
	Scanner scanner = new Scanner(System.in);
	
	public int giveMeFirst() {
		System.out.println("Give me frist number");
		return scanner.nextInt();
	}
	
	public int giveMeSecond() {
		System.out.println("Give me second number");
		return scanner.nextInt();
	}
	
	public int giveMeYourOption() {
		System.out.println("Give me your otion");
		return scanner.nextInt();
	}
	
	public char giveMeYourOptionChar(String options) {
		System.out.println("Give me your option");
		System.out.println(options);
		return scanner.next().charAt(0);
	}
	
	public String doYouWishContinue() {
		System.out.println("Do you wish continue? <<y>> <<n>>");
		return scanner.next();
	}
	
	public String otraTabla() {
		System.out.println("Quieres hacer otra tabla? <<y>> <<n>>");
		return scanner.next();
	}
	public String otroMovimiento() {
		System.out.println("Desea hacer otro movimiento? <<y>> <<n>>");
		return scanner.next();
	}
	
}
